export interface Post {
  id?:number;
  title?:string;
  body?:string;
  author?:string;
  createdAt?:Date;
  comments?:Comment[];
}

export interface Comment {
  id?:number;
  body?:string;
  author?:string;
  createdAt?:Date;
  post?:Post;
}
