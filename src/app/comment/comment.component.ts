import {Component, Input, OnInit} from '@angular/core';
import {CommentService} from "../comment.service";
import {Comment} from "../entities";


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  comments:Comment[] = [];

  @Input()
  postId?:number;

  constructor(private comService:CommentService) { }

  ngOnInit(): void {
    // ????
    this.comService.getByPost(this.postId!).subscribe(data => this.comments = data);
  }

}
