import { Component, OnInit } from '@angular/core';
import { Comment } from '../entities'

@Component({
  selector: 'app-comment-create',
  templateUrl: './comment-create.component.html',
  styleUrls: ['./comment-create.component.css']
})
export class CommentCreateComponent implements OnInit {

  comment?:Comment;

  constructor() { }

  ngOnInit(): void {
  }

}
