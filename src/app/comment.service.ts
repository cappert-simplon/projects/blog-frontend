import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Comment} from "./entities";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Comment[]>(environment.ApiUrl + 'comments/');
  }

  getById(id:number) {
    return this.http.get<Comment>(environment.ApiUrl + 'comments/' + id);
  }

  getByPost(postId:number) {
    return this.http.get<Comment[]>(environment.ApiUrl + 'comments/post/' + postId);
  }

  add(comment:Comment) {
    return this.http.post<Comment>(environment.ApiUrl + 'comments/', comment);
  }

  update(comment:Comment) {
    return this.http.put<Comment>(environment.ApiUrl + "comments/" + comment.id, comment)
  }

  delete(id:number) {
    return this.http.delete<Comment>(environment.ApiUrl + 'comments/' + id);
  }
}
