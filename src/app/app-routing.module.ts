import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PostListComponent} from "./post-list/post-list.component";
import {PostSingleComponent} from "./post-single/post-single.component";

const routes: Routes = [
  { path: '', component: PostListComponent },
  // { path: '/:id', component: PostSingleComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
