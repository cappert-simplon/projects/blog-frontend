import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PostService} from "../post.service";
import {Post} from "../entities";

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css']
})
export class PostEditComponent implements OnInit {
  // ???
  post?: Post;

  constructor(private route: ActivatedRoute, private postService: PostService) {
    // TODO: Post est une interface donc tu peux pas faire comme ça
    // this.post = new Post();
  }

  ngOnInit(): void {
    this.postService.add(this.post!).subscribe()
  }

}
