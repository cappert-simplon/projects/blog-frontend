import { Component, OnInit } from '@angular/core';
import {Post} from "../entities";
import {PostService} from "../post.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-post-single',
  templateUrl: './post-single.component.html',
  styleUrls: ['./post-single.component.css']
})
export class PostSingleComponent implements OnInit {

  post?:Post;

  constructor(private postService:PostService, private ar:ActivatedRoute) { }

  ngOnInit(): void {
    //needed?
    this.post!.comments = [];

    // ????
    this.ar.params.subscribe(param => {this.postService.getById(param['id']).subscribe(data => this.post = data)})
  }

}
