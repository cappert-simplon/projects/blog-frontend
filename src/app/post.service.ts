import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Post} from "./entities";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Post[]>(environment.ApiUrl + 'posts/');
  }

  getById(id:number) {
    return this.http.get<Post>(environment.ApiUrl + 'posts/' + id);
  }

  add(post:Post) {
    return this.http.post<Post>(environment.ApiUrl + 'posts/', post);
  }

  update(post:Post) {
    return this.http.put<Post>(environment.ApiUrl + "posts/" + post.id, post)
  }

  delete(id:number) {
    return this.http.delete<Post>(environment.ApiUrl + 'posts/' + id);
  }
}
