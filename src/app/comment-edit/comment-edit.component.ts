import { Component, OnInit } from '@angular/core';
import { Comment } from '../entities'

@Component({
  selector: 'app-comment-edit',
  templateUrl: './comment-edit.component.html',
  styleUrls: ['./comment-edit.component.css']
})
export class CommentEditComponent implements OnInit {
  comment?: Comment;

  constructor() { }

  ngOnInit(): void {
  }

}
